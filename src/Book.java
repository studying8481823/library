// Represents a book in library
public class Book {
    //@classInvariant:
    //title, author, publisher cannot be null or empty
    //publicationYear must be a positive integer
    //booksAvailability must be a boolean value
    public String title;
    public String author;
    public int publicationYear;
    public String publisher;
    public boolean booksAvailability;

    public Book (String title, String author, int publicationYear, String publisher, boolean booksAvailability) {
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.publisher = publisher;
        this.booksAvailability = booksAvailability;
    }

    // getters
    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public String getPublisher() {
        return publisher;
    }

    public boolean isBooksAvailability() {
        return booksAvailability;
    }

    //setters
    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setBooksAvailability(boolean booksAvailability) {
        this.booksAvailability = booksAvailability;
    }
}
