import java.util.ArrayList;

// Represents some kind of DB in the library for
public class Catalog {
    // mainCatalog: contains only valid Book-objects, used directly just by librarians
    // librarians: contains unique Librarian-objects, used to identify librarians
    // clients: contains unique Client-objects, used to identify clients
    private final ArrayList <Book> mainCatalog = new ArrayList<>();
    private final ArrayList <Librarian> librarians = new ArrayList<>(); // could be used later
    private final ArrayList <Client> clients = new ArrayList<>(); // could be used later

    // adds librarian to the librarians
    // @.pre librarian is not in librarians
    // @.post added new librarian to librarians
    public void addLibrarian (Librarian librarian) {
        librarians.add(librarian);
    }

    // finds librarian by Id
    public Librarian findLibrarianById (int librarianId) {
        for (Librarian l: librarians) {
            if (l.getLibrarianId() == librarianId) {
                return l;
            }
        }
        return null;
    }

    // adds a valid Book-object to mainCatalog
    // can be used only alongside with valid librarianId to protect the integrity
    public void addBook (int librarianId, Book book) {
        Librarian librarian = findLibrarianById(librarianId);
        if (librarian != null) {
            mainCatalog.add(book);
            System.out.println("Book added.");
        } else {
            System.out.println("Librarian not found.");
        }
    }

    // changes book's Availability to opposite
    public void booksAvailabilityChange (int librarianId, Book book) {
        Librarian librarian = findLibrarianById(librarianId);
        if (librarian != null) {
            book.setBooksAvailability(!book.isBooksAvailability());
            System.out.println("Book availability changed.");
        } else {
            System.out.println("Librarian not found.");
        }
    }

    // seeks for the Book-object in the mainCatalog by title and author that it receives as parameters
    public Book findBookByTitleAndAuthor(String title, String author) {
        for (Book book : mainCatalog) {
            if (book.getTitle().equals(title) && book.getAuthor().equals(author)) {
                return book;
            }
        }
        return null;
    }

    // adds client to the clients
    // @.pre client is not in clients
    // @.post added new client to clients
    public void addClient (int librarianId, int clientId, String password) {
        Librarian librarian = findLibrarianById(librarianId);
        if (librarian != null) {
            Client client = new Client(clientId, password);
            clients.add(client);
            System.out.println("Client added.");
        } else {
            System.out.println("Librarian not found.");
        }
    }

    // returns a mainCatalog, used for encapsulation
    public ArrayList<Book> getCatalog() {
        return mainCatalog;
    }

    //not sure if it needs to be used ATM, maybe later can be made
    public boolean loginLibrarian(int librarianId, String password) {
        for (Librarian l : librarians) {
            if (l.getLibrarianId() == librarianId && l.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    //not sure if it needs to be used ATM, maybe later with the loaning methods
    public boolean loginClient(int clientId, String password) {
        for (Client c : clients) {
            if (c.getClientId() == clientId && c.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

}
