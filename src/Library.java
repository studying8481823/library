import java.util.ArrayList;

// Represents a library that uses a catalog
public class Library {
    private Catalog catalog;
    private ArrayList <Book> deepCopy;

    public Library(Catalog catalog) {
        this.catalog = catalog;
        this.deepCopy = null;
    }

    // updates the deep copy of the catalog
    public void updateDeepCopyCatalog() {
        ArrayList<Book> originalCatalog = catalog.getCatalog();
        deepCopy = new ArrayList<>();

        for (Book b : originalCatalog) {
            deepCopy.add(new Book(b.getTitle(), b.getAuthor(), b.getPublicationYear(), b.getPublisher(), b.isBooksAvailability()));
        }

    }

    // returns the deep copy of the catalog
    public ArrayList<Book> getCachedDeepCopy() {
        if (deepCopy == null){
            updateDeepCopyCatalog();
        }

        return deepCopy;
    }

}
