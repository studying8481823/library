import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        Catalog catalog = new Catalog();
        catalog.addLibrarian(new Librarian(549219, "Bestlibrarian"));
        catalog.addBook(549219, new Book("How to write a book", "Vsevolod Bondar", 2024, "Family", true));

        Library library = new Library(catalog);

        boolean exit = false;

        while (!exit) {
            System.out.println("\n Choose an operation:");
            System.out.println("1 - Look at the book's catalogue.");
            System.out.println("2 - Add a book (librarians only).");
            System.out.println("3 - Change book's availability (librarians only).");
            System.out.println("4 - Update the catalogue");
            System.out.println("5 - Exit.");

            int variant = Integer.parseInt(reader.nextLine());

            switch (variant) {
                case 1:
                    lookAtCatalogue(library);
                    break;
                case 2:
                    System.out.print("Enter librarian ID: ");
                    int librarianId = Integer.parseInt(reader.nextLine());
                    addBook(reader, catalog, librarianId);
                    break;
                case 3:
                    System.out.print("Enter librarian ID: ");
                    librarianId = Integer.parseInt(reader.nextLine());
                    changeBookAvailability(reader, catalog, librarianId);
                    break;
                case 4:
                    library.updateDeepCopyCatalog();
                    break;
                case 5:
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
            }
        }
        reader.close();
    }

    private static void lookAtCatalogue(Library library) {
        ArrayList<Book> catalogForClient = library.getCachedDeepCopy();
        System.out.println("Books in the catalog:");
        for (Book book : catalogForClient) {
            System.out.println("Title: " + book.getTitle() + ", Author: " + book.getAuthor() +
                    ", Year: " + book.getPublicationYear() + ", Publisher: " + book.getPublisher() +
                    ", Available: " + book.isBooksAvailability());
        }
    }

    private static void addBook(Scanner reader, Catalog catalog, int librarianId) {
        System.out.print("Enter book title: ");
        String title = reader.nextLine();
        System.out.print("Enter book author: ");
        String author = reader.nextLine();
        System.out.print("Enter publication year: ");
        int publicationYear = Integer.parseInt(reader.nextLine());
        System.out.print("Enter publisher: ");
        String publisher = reader.nextLine();
        System.out.print("Is the book available (true/false): ");
        boolean availability = Boolean.parseBoolean(reader.nextLine());

        Book book = new Book(title, author, publicationYear, publisher, availability);
        catalog.addBook(librarianId, book);
    }

    private static void changeBookAvailability(Scanner reader, Catalog catalog, int librarianId) {
        // better to have some kind of book's ID of course, but we use these for now
        System.out.print("Enter book title: ");
        String title = reader.nextLine();
        System.out.print("Enter book author: ");
        String author = reader.nextLine();

        Book book = catalog.findBookByTitleAndAuthor(title, author);
        if (book != null) {
            catalog.booksAvailabilityChange(librarianId, book);
        } else {
            System.out.println("Book not found.");
        }
    }
}
