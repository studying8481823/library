In the library's book database, objects of type "Book" are stored. Assume that a book has typical fields such as title, publication year, publisher, etc.

The library's customers and staff can make queries in the book database to search for specific books. Additionally, the database supports updates to the data by library staff. Why is this? For example, there may have been initial errors in the data, or it might be necessary to remove a book from the collection, mark it as lost, etc.

 

(In reality, the database would likely be stored on disk and use some type of database engine, but let's assume here that the database is always entirely in the memory of a Java process as simple objects, and the state of the objects is saved back to a file at the end of the day and reloaded into memory at the beginning of the next day. All data processing would thus simply take place with objects in the memory of the Java process.)

 

Task: What principle of data protection would you choose in the case of the Book type when you want to serve the two different roles mentioned:

 A library customer makes a search query and you want to offer the customer a view that only supports reading the books.

Library staff want to permanently edit the information of a specific book.

Describe the mechanisms you choose and their consequences for class features, class invariant, and usage. It will suffice to focus mainly on the Book type. For simplicity, we can assume that the database is an array (Book[]) or a list (List<Book>) of books (you can choose either - arrays should be familiar to you from previous courses). You may also implement the program if you wish, but just creating the specifications is enough.

 