public class Librarian {
    //@.classInvariant: clientId != null && clientId.length = 6
    //password != null
    private int librarianId;
    private String password;

    public Librarian (int librarianId, String password) {
        this.librarianId = librarianId;
        this.password = password;
    }

    public int getLibrarianId() {
        return librarianId;
    }

    public String getPassword() {
        return password;
    }

    public void setLibrarianId(int librarianId) {
        this.librarianId = librarianId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
