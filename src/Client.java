public class Client {
    //@.classInvariant: clientId != null && clientId.length = 6
    // password != null
    private int clientId;
    private String password;

    public Client (int clientId, String password) {
        this.clientId = clientId;
        this.password = password;
    }

    public int getClientId() {
        return clientId;
    }

    public String getPassword() {
        return password;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


